({
    doInit: function(component, event, helper) {
        var today = new Date();
        component.set('v.myDate', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
    },

    afterScriptsLoaded: function(cmp, event, helper) {
        helper.fetchCalenderEvents(cmp);
    },

    handleClick: function (component, event, helper) {

        var buttonstate = component.get("v.buttonstate");
        component.set("v.buttonstate", !buttonstate);
        if (!buttonstate) {
            $("#listcalendar").show();
            $("#calendar").hide();
            $('#listcalendar').fullCalendar({
                defaultView: 'listWeek',
                listDayFormat: true,
                events: component.get("v.Objectlist")
            });

        }
        else {
            $("#calendar").show();
            $("#listcalendar").hide();
            helper.fetchCalenderEvents(component);
        }

    },

    handleSelect: function (cmp, event) {
        // This will contain the string of the "value" attribute of the selected
        // lightning:menuItem
        var selectedMenuItemValue = event.getParam("value");
        alert("Menu item selected with value: " + selectedMenuItemValue);
    },

    openMenu: function (cmp, event) {
        // This will contain the string of the "value" attribute of the selected
        // lightning:menuItem
        if (cmp.get("v.MenuOpen")==false){
            cmp.set("v.MenuOpen", true); 
        }
        else 
            cmp.set("v.MenuOpen", false);
        
    },

});
