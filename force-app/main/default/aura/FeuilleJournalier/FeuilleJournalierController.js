({
    doInit : function(component, event, helper) {
        var today = new Date();
        component.set('v.today', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
    },

    openPause: function (cmp, event) {
        // This will contain the string of the "value" attribute of the selected
        // lightning:menuItem
        if (cmp.get("v.pauseView")==false){
            cmp.set("v.pauseView", true); 
        } else {
            cmp.set("v.pauseView", false);
            if (cmp.get("v.heureSortiePause") && cmp.get("v.heureRetourPause"))
                cmp.set("v.breakHourSet", true);
        }
        
    },

    openExportImportModal: function(component, event, helper) {
        component.set("v.ModalImportExport", true);
      },
    
      closeModelImportExport: function(component, event, helper) {
        component.set("v.ModalImportExport", false);
      },
})