({
    init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            { label: 'Employé name', fieldName: 'employeName', type: 'text', editable: true, typeAttributes: { required: true } },
            { label: 'Date', fieldName: 'date', type: 'text', editable: false },
            { label: "Heure d'entrée", fieldName: 'heureEntree', type: 'text', editable: false },
            { label: 'Heure de sortie', fieldName: 'heureSortie', type: 'text', editable: false },
            { label: "Heure de sortie pause", fieldName: 'heureSortiePause', type: 'text', editable: false },
            { label: "Heure de retour du pause", fieldName: 'heureRetourPause', type: 'text', editable: false },
            { label: "Durée travail", fieldName: 'dureeTravail', type: 'text', editable: false },
            { label: "Durée du pause", fieldName: 'dureePause', type: 'text', editable: false },
            /* {label: "Validation", fieldName: 'validation' , type: 'button',variant="success", editable: true},*/
            {
                label: 'Validation',
                type: 'button',
                typeAttributes: {
                        iconName: 'action:approval',
                        label: 'Valider',
                        name: 'btnValider',
                        disabled: false,       
                        value: 'validationBtn'},
            },
            {label: 'Action', type: 'button', initialWidth: 150, typeAttributes:
            { label: { fieldName: 'actionLabel'}, title: 'Click to Edit', name: 'edit_status', iconName: 'utility:edit', disabled: {fieldName: 'actionDisabled'}, class: 'btn_next'}}
        ]);

cmp.set('v.data', [
    /*{employeName: 'GAIED Mohamed'},{Date: 'GAIED Mohamed'},{heureEntree:"08:15"},{heureSortie: "17:15"}, heureSortiePause,"08:15" ,heureRetourPause: "17:15"},*/
    { employeName: 'GAIED Mohamed', date: "25/11/2019", heureEntree: "08:15", heureSortie: "17:15", heureSortiePause: "12:00", heureRetourPause: "13:00", dureeTravail: "8,00", dureePause: "1,00", actionLabel: "Valider" },
    { employeName: 'GAIED Mohamed', date: "24/11/2019", heureEntree: "08:15", heureSortie: "17:15", heureSortiePause: "12:00", heureRetourPause: "13:00", dureeTravail: "8,00", dureePause: "1,00" , actionLabel: "Valider"},
    { employeName: 'GAIED Mohamed', date: "11/11/2019", heureEntree: "08:15", heureSortie: "17:15", heureSortiePause: "12:00", heureRetourPause: "13:00", dureeTravail: "8,00", dureePause: "1,00", actionLabel: "Valider" },
    { employeName: 'GAIED Mohamed', date: "10/11/2019", heureEntree: "08:15", heureSortie: "17:15", heureSortiePause: "12:00", heureRetourPause: "13:00", dureeTravail: "8,00", dureePause: "1,00", actionLabel: "Valider"},
    { employeName: 'GAIED Mohamed', date: "07/11/2019", heureEntree: "08:15", heureSortie: "17:15", heureSortiePause: "12:00", heureRetourPause: "13:00", dureeTravail: "8,00", dureePause: "1,00", actionLabel: "Valider" },
    { employeName: 'GAIED Mohamed', date: "06/11/2019", heureEntree: "08:15", heureSortie: "17:15", heureSortiePause: "12:00", heureRetourPause: "13:00", dureeTravail: "8,00", dureePause: "1,00", actionLabel: "Valider" },
    { employeName: 'GAIED Mohamed', date: "05/11/2019", heureEntree: "08:15", heureSortie: "17:15", heureSortiePause: "12:00", heureRetourPause: "13:00", dureeTravail: "8,00", dureePause: "1,00", actionLabel: "Valider" },
    { employeName: 'GAIED Mohamed', date: "03/11/2019", heureEntree: "08:15", heureSortie: "17:15", heureSortiePause: "12:00", heureRetourPause: "13:00", dureeTravail: "8,00", dureePause: "1,00", actionLabel: "Valider" },
    { employeName: 'GAIED Mohamed', date: "02/11/2019", heureEntree: "08:15", heureSortie: "17:15", heureSortiePause: "12:00", heureRetourPause: "13:00", dureeTravail: "8,00", dureePause: "1,00", actionLabel: "Valider" },
    { employeName: 'GAIED Mohamed', date: "01/11/2019", heureEntree: "08:15", heureSortie: "17:15", heureSortiePause: "12:00", heureRetourPause: "13:00", dureeTravail: "8,00", dureePause: "1,00", actionLabel: "Valider" }

]);

        /*helper.initServer().then($A.getCallback(function () {
            helper.fetchData(cmp);
        }));*/
    },
handleSaveEdition: function (cmp, event, helper) {
    var draftValues = event.getParam('draftValues');

    helper.saveEdition(cmp, draftValues);
},
handleCancelEdition: function (cmp) {
    // do nothing for now...
}
});
